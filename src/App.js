import React, { Component } from "react";
import { connect } from "react-redux";
import "./App.css";
import {
  getItemRequest,
  addItemRequest,
  editItemRequest,
  deleteItemRequest,
} from "./action/Todos";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      age: "",
      email: "",
      phone: "",
      id: "", // variable check handle update , add data
      validateName: false, // variable check handle validate input name
    };
  }
  handleChangeInput = (e) => {
    //fn change input data
    this.setState({
      [e.target.name]: e.target.value,
      validateName: false,
    });
  };
  handleSubmit = () => {
    //fn handle submit && edit data
    if (!this.state.name) {
      this.setState({
        validateName: true,
      });
    } else if (this.state.id !== "") {
      // check index if value !=="" edit item
      const item = {
        name: this.state.name,
        age: this.state.age,
        email: this.state.email,
        phone: this.state.phone,
      };

      this.props.editItemRequest(this.state.id, item);
      this.setState({
        name: "",
        age: "",
        email: "",
        phone: "",
        id: "",
      });
    } else {
      // check index if value !=="" add item
      const item = {
        name: this.state.name,
        age: this.state.age,
        email: this.state.email,
        phone: this.state.phone,
      };
      this.props.addItemRequest(item);
      this.setState({
        name: "",
        age: "",
        email: "",
        phone: "",
        id: "",
      });
    }
  };
  handleCancel = () => {
    this.setState({
      name: "",
      age: "",
      email: "",
      phone: "",
      id: "",
    });
  };
  handleEdit = (item) => {
    //fn handle get data edit
    this.setState({
      name: item.name,
      age: item.age,
      email: item.email,
      phone: item.phone,
      id: item.id,
    });
  };
  handleDelete = (id) => {
    //fn handle delete
    this.props.deleteItemRequest(id);
    this.setState({
      name: "",
      age: "",
      email: "",
      phone: "",
      id: "",
    });
  };
  componentDidMount() {
    this.props.getItemRequest();
  }
  render() {
    return (
      <div className="App">
        <div className="content-input">
          <label>Name :</label>
          <input
            type="text"
            className="form-control"
            name="name"
            value={this.state.name}
            onChange={this.handleChangeInput}
          />
          {this.state.validateName && (
            <p className="validate"> Tên không được để trống</p>
          )}
        </div>
        <div className="content-input">
          <label>Age :</label>
          <input
            type="text"
            className="form-control"
            name="age"
            value={this.state.age}
            onChange={this.handleChangeInput}
          />
        </div>
        <div className="content-input">
          <label>Email :</label>
          <input
            type="text"
            className="form-control"
            name="email"
            value={this.state.email}
            onChange={this.handleChangeInput}
          />
        </div>
        <div className="content-input">
          <label>phone :</label>
          <input
            type="text"
            className="form-control"
            name="phone"
            value={this.state.phone}
            onChange={this.handleChangeInput}
          />
        </div>
        <button
          type="button"
          className="btn btn-success"
          onClick={this.handleSubmit}
        >
          Success
        </button>
        <button
          type="button"
          className="btn btn-danger"
          onClick={this.handleCancel}
        >
          Cancel
        </button>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Age</th>
              <th scope="col">Email</th>
              <th scope="col">phone</th>
            </tr>
          </thead>
          <tbody>
            {this.props.listData.map((item, index) => (
              <tr key={index}>
                <th scope="row">{item.id}</th>
                <td>{item.name}</td>
                <td>{item.age}</td>
                <td>{item.email}</td>
                <td>{item.phone}</td>
                <td>
                  <button onClick={() => this.handleEdit(item)}>Edit</button>
                  <button onClick={() => this.handleDelete(item.id)}>
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    listData: state.Todos.listData,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getItemRequest: () => dispatch(getItemRequest()),
    addItemRequest: (item) => dispatch(addItemRequest(item)),
    editItemRequest: (index, item) => dispatch(editItemRequest(index, item)),
    deleteItemRequest: (id) => dispatch(deleteItemRequest(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
