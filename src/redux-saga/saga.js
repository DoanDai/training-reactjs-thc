import { all } from "redux-saga/effects";
import { todos } from "../api/Todos/redux/redux-saga";

function* rootSaga() {
  yield all([...todos]);
}
export default rootSaga;
