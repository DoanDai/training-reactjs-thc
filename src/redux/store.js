import { createStore, applyMiddleware, compose } from "redux";

import { combineReducers } from "redux";

import Todos from "../reducer/Todos";

import rootSaga from "../redux-saga/saga";
import createSagaMiddleware from "redux-saga";
//root reducer
const initialState = {};

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// dev tools middleware
const reduxDevTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export const store = createStore(
  combineReducers({ app: rootReducer }),
  compose(applyMiddleware(sagaMiddleware), reduxDevTools)
);
// run the saga
sagaMiddleware.run(rootSaga);

const asyncReducers = {};

const getNewReducer = (name, reducer) => {
  asyncReducers[name] = reducer;

  store.replaceReducer(
    combineReducers({
      app: rootReducer,
      ...asyncReducers,
    })
  );
};

// declare the reducer
getNewReducer("Todos", Todos);
