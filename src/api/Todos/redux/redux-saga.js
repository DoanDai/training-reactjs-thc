import { put, takeEvery, call } from "redux-saga/effects";
import ActionTypes from "../../../type/type";

import { getTodos, addItem, editItem, deleteItem } from "./Todos";

function* getTodosRequest() {
  try {
    const res = yield getTodos();
    yield put({
      type: ActionTypes.GET_LIST_SUCCESS,
      payload: res.data, //có dữ liệu thì lưu ở đây
    });
  } catch (error) {
    yield put({
      type: ActionTypes.GET_LIST_FAILED,
      error: error, //có dữ liệu thì lưu ở đây
    });
  }
}

function* addItemRequest(action) {
  try {
    const res = yield call(addItem, action.payload);
    if (res.status === 201) {
      yield put({
        type: ActionTypes.ADD_ITEM_SUCCESS,
        payload: res.data, //có dữ liệu thì lưu ở đây
      });
    } else {
      yield put({
        type: ActionTypes.ADD_ITEM_FAILED,
        error: res.error, // Bao loi
      });
    }
  } catch (error) {
    yield put({
      type: ActionTypes.ADD_ITEM_FAILED,
      error: error, // Bao loi
    });
  }
}

function* editItemRequest(action) {
  try {
    const res = yield call(editItem, action.id, action.payload);

    if (res.status === 200) {
      yield put({
        type: ActionTypes.EDIT_ITEM_SUCCESS,
        payload: res.data, //có dữ liệu thì lưu ở đây
      });
    } else {
      yield put({
        type: ActionTypes.EDIT_ITEM_FAILED,
        error: res.error, // Bao loi
      });
    }
  } catch (error) {
    yield put({
      type: ActionTypes.EDIT_ITEM_FAILED,
      error: error, // Bao loi
    });
  }
}
function* deleteItemRequest(action) {
  try {
    const res = yield call(deleteItem, action.payload);

    if (res.status === 200) {
      yield put({
        type: ActionTypes.DELETE_ITEM_SUCCESS,
        payload: res.data, //có dữ liệu thì lưu ở đây
      });
    } else {
      yield put({
        type: ActionTypes.DELETE_ITEM_FAILED,
        error: res.error, // Bao loi
      });
    }
  } catch (error) {
    yield put({
      type: ActionTypes.DELETE_ITEM_FAILED,
      error: error, // Bao loi
    });
  }
}

export const todos = [
  // nhận connect từ action
  takeEvery(ActionTypes.GET_LIST_REQUEST, getTodosRequest),
  takeEvery(ActionTypes.ADD_ITEM_REQUEST, addItemRequest),
  takeEvery(ActionTypes.EDIT_ITEM_REQUEST, editItemRequest),
  takeEvery(ActionTypes.DELETE_ITEM_REQUEST, deleteItemRequest),

  // nhan tu ben saga thấy request thì gọi get list request
];
