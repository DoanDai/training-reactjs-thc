import apiBase from "../../../baseAPI";

export const getTodos = () => {
  return new Promise((resolve, reject) => {
    return apiBase
      .get("/TodoApp")
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

export const addItem = (item) => {
  return new Promise((resolve, reject) => {
    return apiBase
      .post("/TodoApp", item)
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

export const editItem = (id, item) => {
  return new Promise((resolve, reject) => {
    return apiBase
      .put(`/TodoApp/${id}`, item)
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

export const deleteItem = (id) => {
  return new Promise((resolve, reject) => {
    return apiBase
      .delete(`/TodoApp/${id}`)
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};
