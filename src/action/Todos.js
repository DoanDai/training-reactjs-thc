import ActionTypes from "../type/type";

export function getItemRequest() {
  return {
    type: ActionTypes.GET_LIST_REQUEST,
  };
}

export function addItemRequest(data) {
  return {
    type: ActionTypes.ADD_ITEM_REQUEST,
    payload: data,
  };
}
export function editItemRequest(id, data) {
  return {
    type: ActionTypes.EDIT_ITEM_REQUEST,
    payload: data,
    id,
  };
}
export function deleteItemRequest(index) {
  return {
    type: ActionTypes.DELETE_ITEM_REQUEST,
    payload: index,
  };
}
