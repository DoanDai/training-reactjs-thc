import ActionTypes from "../type/type";

const initialState = {
  listData: [],
};
export default function Todos(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.GET_LIST_SUCCESS:
      return {
        ...state,
        listData: [...action.payload],
      };
    case ActionTypes.GET_LIST_FAILED:
      return {
        ...state,
      };
    case ActionTypes.ADD_ITEM_SUCCESS:
      return {
        ...state,
        listData: [...state.listData, action.payload],
      };
    case ActionTypes.ADD_ITEM_FAILED:
      return { ...state };

    case ActionTypes.EDIT_ITEM_SUCCESS:
      //handle find index select in arr listData
      const indexSelect = state.listData.findIndex(
        (item) => item.id === action.payload.id
      );
      state.listData.splice(indexSelect, 1, action.payload);
      return {
        ...state,
        listData: [...state.listData],
      };
    case ActionTypes.EDIT_ITEM_FAILED:
      return { ...state };

    case ActionTypes.DELETE_ITEM_SUCCESS:
      //handle find index select in arr listData
      const indexSelectDelete = state.listData.findIndex(
        (item) => item.id === action.payload.id
      );
      state.listData.splice(indexSelectDelete, 1);
      return {
        ...state,
        listData: [...state.listData],
      };
    case ActionTypes.DELETE_ITEM_FAILED:
      return { ...state };
    default:
      return state;
  }
}
